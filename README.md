# Installation
Requires Node.js and Yarn.

After installation, clone the repo.

cd into my-app

    cd my-app

Use Yarn to download dependencies

    yarn

Then start the project's development environment to explore

    yarn start


# Notes
I used AWS Lambdas endpoints since I didn't want to write a whole new backend to handle this. 

Inital set up was easy. However I'm new to Typescript so implementing some things was more difficult than previously thought (I'm more experience with JS). 

CORS was a big issue trying to get these endpoints and basic functions working. None of this was an issue with the Flask app. 

Hopefully this will not occur in the other projects.

Build took 10.31 seconds to complete. 
Build is 696KB. 
