import axios from 'axios'

export async function getPostList()
{
  let res = await axios({
      method: 'GET',
      url: 'https://024bfcwstk.execute-api.us-east-1.amazonaws.com/api/get-items',
      headers: {
          'Content-Type': 'application/json'
      }
  })
  let data = res.data.Items;
  return data
}

export async function addPost(name, message)
{
    let body = {
        name: name,
        message: message
    }
    console.log(body)
    let res = await axios({
        method: 'POST',
        url: 'https://024bfcwstk.execute-api.us-east-1.amazonaws.com/api/add-item',
        headers: {
            'Content-Type': 'application/json',
        },
        data: body
    })

    window.location.reload(false)

    return res
}