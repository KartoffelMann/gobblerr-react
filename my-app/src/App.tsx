import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { addPost } from './Helpers'
import './App.css';


function App() {

  let [data, setData]: any = useState({});
  let [name, setName] = useState('');
  let [message, setMessage] = useState('')

  const [isLoading, setIsLoading] = useState(false);

  const handleName = (event: any) => {
    const value = event.target.value
    setName(value)
  }

  const handleMessage = (event: any) => {
    const value = event.target.value
    setMessage(value)
  }

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      const result = await axios({
        method: 'GET',
        url: 'https://024bfcwstk.execute-api.us-east-1.amazonaws.com/api/get-items',
        headers: {
            'Content-Type': 'application/json'
        }
      });
      setData(result.data.Items) 
      setIsLoading(false)
    };

    fetchData();
  }, [])

  return (
    <div>
      <div>
        <h1 id="title">Gobbler App</h1>
      </div>

      <br></br>
        <div className="form">
          <form>
              <label>
                Name:  
                <input type='text' name='name' onChange={handleName} />
              </label>
              <br></br>
              <label>
                Message:  
                <input type='text' name='message'  onChange={handleMessage} />
              </label>
          </form>
          <button onClick={() => addPost(name, message)}>Submit</button>
        </div>


      <br></br>

      <div className='messages'>
        <ul>
          {data && Object.keys(data).map(key => {
            return (
              <div key={key}>
                <h2>{data[key].AccountName}</h2>
                <p>{data[key].Message}</p>
                <p>{data[key].Timestamp}</p>
              </div>
            )
          })}
        </ul>

      </div>
  </div>

  );
} 

export default App;


